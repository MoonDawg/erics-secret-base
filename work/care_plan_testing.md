# Care Plan Testing Checklist

## [Patient Record](https://testing.aspirehealth.io/patient-record/0011C00002Lbg05QAB/care-plan)

#### Patient Overview

- Verify that patient overview renders correctly (if it exists)

### Previous Care Plan

- Verify that previous care plan renders correctly (if it exists)

### Problems List

- Add Problem
  - Methods
    - Add via problem search
    - Add via dropdowns
    - Add custom problem
  - Diagnosis
  - Status
  - Onset Date
  - Notes
- Edit Problem
  - Diagnosis
  - Status
  - Onset Date
  - Notes
- Problem Details
  - Add/edit problem overview
  - Add normal problem detail (if applicable)
  - View detail history (if applicable)
- Change problem rankings

<br />

## Assessments

### Side Panel

- Click on Previous Assessment and verify that patient overview and assessment and plan render correctly (if they exist)

### Last Care Plan

- Verify that previous care plan renders correctly (if it exists)

### Problems List

- Test everything that you would on the patient record problems list
- Adding a problem via High Priority Problem Type Selection (if applicable)
- Adding a problem via ePAHAF (if applicable)
- Changing a status via the dropdown on the grid

### Care Plans

- Select problems, goals, and actions to form care plan
- Select an action that has automated follow up (indicated by lightning bolt)
- Create a custom goal and action
- Add problem/action assessment notes
- Verify that it fails to validate if you leave off required information
- Verify that actions with automated follow up are represented on the encounter manifest
- Sign encounter and verify that the changes are represented on patient record

### Downloading Assessment

- Verify that problems list looks correct
- Verify that problem overview looks correct
- Verify that care plan looks correct

<br />

## Encounter Reviews

### Care Plans

- Add/edit problem overview
- Change problem rankings
- Edit supplements (if applicable)
- Edit problem/action assessment notes
- Preview MD Report and verify that care plan looks correct
- Sign and Defer to send it to IOC Dashboard

<br />

## IOC Dashboard

- Verify that problem overview looks correct
- Verify that care plan looks correct

<br />

## MD Reports

- Verify that the patient overview looks correct
- Verify that the care plan looks correct

<br />

## [Partner Portal](https://testingpartner.aspirehealth.io/patient-list)

### Patient Overview

- Verify that patient overview renders correctly (if it exists)

### Previous Care Plan

- Verify that previous care plan renders correctly (if it exists)

### Problems List

- Verify that problems list renders correctly (if they have any)
