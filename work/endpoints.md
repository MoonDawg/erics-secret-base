# Telehealth OS Endpoints

### Create Appointment

`POST /appointment/session/{participant_uuid} `

### Delete Appointment

`DELETE ​/patient​/{patient_uuid}​/appointment​/{id}`

### Edit Patient Information

`PATCH /practice/patient/{uid}`
