# TelehealthOS API Notes

## Appointment Endpoints

### POST

`/api/practice/c3990bd9-8fdb-40b0-9df1-c66b975ff7e8/appointments`

Request

```json
{
  "host": "c3990bd9-8fdb-40b0-9df1-c66b975ff7e8",
  "duration": "45:00",
  "start_at": "2020-12-10T22:45:00.000Z",
  "practice": "915bcf2a-8b67-4059-bc1a-55b77d3112e7",
  "participants": [
    {
      "id": "a95acce3-1a50-45da-9bb2-52fe93384ce6",
      "object_type": "patient"
    }
  ]
}
```

Response

```json
{
  "id": "f119d294-f9fe-4a2f-ba53-8545ff6bda22",
  "participants": [
    {
      "id": "d2878198-4774-4369-8d4f-ae8f11bfa3ac",
      "first_name": "first",
      "last_name": "lasttt",
      "email": "moondawg2413@gmail.com",
      "phone": "(865) 755-6609",
      "dob": 19921229,
      "object_type": "patient",
      "object_uid": "a95acce3-1a50-45da-9bb2-52fe93384ce6",
      "is_host": false
    },
    {
      "id": "61b922da-bbc5-4e3c-bf4a-3ac7d237d84d",
      "first_name": "Doctor",
      "last_name": "Eric",
      "email": "emoon@aspirehealthcare.com",
      "phone": "(865) 555-1234",
      "dob": null,
      "object_type": "provider",
      "object_uid": "c3990bd9-8fdb-40b0-9df1-c66b975ff7e8",
      "is_host": true
    }
  ],
  "start_at": "2020-12-10T22:45:00Z",
  "end_at": "2020-12-10T23:30:00Z",
  "slug": "f119d294-f9fe-4a2f-ba53-8545ff6bda22",
  "status": "A",
  "duration": 45,
  "meeting_id": null
}
```

### DELETE

`/api/practice/appointment/f119d294-f9fe-4a2f-ba53-8545ff6bda22`

(Also `/api/patient/{patient_uuid}/appointment/{id}`?)

Request

```json
None
```

Response

```json
None
```

## Patient Endpoints

### POST

`/api/practice/c3990bd9-8fdb-40b0-9df1-c66b975ff7e8/patients`

Request

```json
{
  "dob": 19921229,
  "email": "moondawg2413@gmail.com",
  "first_name": "first",
  "last_name": "last",
  "phone": "(865) 755-6609",
  "phone_type": "MOBILE"
}
```

Response

```json
{
  "accepted_tos": false,
  "created_at": "2020-12-10T22:32:47.103688Z",
  "deleted_at": null,
  "dob": 19921229,
  "email": "moondawg2413@gmail.com",
  "first_name": "first",
  "gender": "C",
  "id": "a95acce3-1a50-45da-9bb2-52fe93384ce6",
  "last_name": "last",
  "opt_in_or_out_last_record": null,
  "phone": "(865) 755-6609",
  "phone_type": "MOBILE",
  "practice": 176,
  "uuid": "a95acce3-1a50-45da-9bb2-52fe93384ce6"
}
```

### PATCH

`/api/practice/patient/a95acce3-1a50-45da-9bb2-52fe93384ce6`

Request

```json
{
  "dob": 19921229,
  "email": "moondawg2413@gmail.com",
  "first_name": "first",
  "id": "a95acce3-1a50-45da-9bb2-52fe93384ce6",
  "last_name": "lasttt",
  "phone": "(865) 755-6609",
  "phone_type": "MOBILE"
}
```

Response

```json
{
  "accepted_tos": false,
  "created_at": "2020-12-10T22:32:47.103688Z",
  "deleted_at": null,
  "dob": 19921229,
  "email": "moondawg2413@gmail.com",
  "first_name": "first",
  "gender": "C",
  "id": "a95acce3-1a50-45da-9bb2-52fe93384ce6",
  "last_name": "lasttt",
  "opt_in_or_out_last_record": null,
  "phone": "(865) 755-6609",
  "phone_type": "MOBILE",
  "practice": 176,
  "uuid": "a95acce3-1a50-45da-9bb2-52fe93384ce6"
}
```

## Table to keep our appointments in sync

- Insert records on creation, reschedule, cancellation, and patient info change
- Have poller query from table to kick off calls to THOS
- Update table with IDs
- Implement fail count to ignore constantly failing events
