# Tech Debt List

- Lazily add epics
- Lazily add reducers
- Make patient tag data structure not suck
  - Remove patientInfo in favor of patient PFAPI
- Standardize PFAPI structure
- Standardize client error handling (get rid of showError/alertOnError)
- Fix up calendar widget
- Standardize client UI design (DevX grid labels, etc.)
- Trimming unneeded dependencies (Polygonator has a few)
- Update dependencies
- Upgrade epics to redux-observable 2.0
- Fixing old APIs to use Request
- Websockets suck?
- Upgrade routes to redux-observable 2.0
- Upgrade MUI 4.0
- Rip redux-observable out of Request
- Get rid of container folders
- Get rid of global data modules
- Switch from prop types to TypeScript

# Lazy Epics To-Do:

- Check task epic creators (will double add for subsequent loads)
- Check which global data/feature epics are used in apps
- Dynamic imports for appEpics
- Link for reference: https://stackoverflow.com/questions/40202074/is-it-an-efficient-practice-to-add-new-epics-lazily-inside-react-router-onenter
- Playground: https://stackblitz.com/edit/redux-observable-v1-playground-347v6j?file=index.js

# Misc

- Patient Record ProblemsList container not used?
- Couple lazy epic load to `navigate` in `createAppRouter`?
