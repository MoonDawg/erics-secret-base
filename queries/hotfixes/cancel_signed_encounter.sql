UPDATE aspire.encounter
SET assessment_status = 'cancelled',
  signed_at = NULL,
  signed_by = NULL,
  cancellation_reason = 'created_in_error'
WHERE id = :encounter_id;

UPDATE salesforce."case"
SET STATUS = 'Cancelled',
  cancelled_by__c = 'Aspire Driven',
  cancelled_date__c = NOW(),
  cancellation_type__c = 'Scheduled in error',
  cancellation_notes__c = 'Administrative error.',
  cancelled_by_user_id__c = :user_id
WHERE external_assessment_id__c = :encounter_id;

DELETE FROM aspire.encounter_review
WHERE encounter_id = :encounter_id;
