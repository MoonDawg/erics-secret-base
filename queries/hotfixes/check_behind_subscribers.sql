-- Checking on subscribers
SELECT s.stream_uuid,
  sub.subscription_name,
  stream_version - sub.last_seen AS behind
FROM public.subscriptions sub
  JOIN public.streams s ON s.stream_uuid = sub.stream_uuid
WHERE stream_version - sub.last_seen > 0
ORDER BY behind DESC;

-- Check for bad event
SELECT s.stream_uuid,
  s.subscription_name,
  s.last_seen,
  se.stream_version,
  e.*
FROM subscriptions s
  JOIN streams st ON st.stream_uuid = s.stream_uuid
  JOIN stream_events se ON se.stream_id = st.stream_id
  AND se.stream_version = (s.last_seen + 1)
  JOIN EVENTS e ON e.event_id = se.event_id
WHERE s.subscription_name = 'Elixir.PatientForms.Subscriber';