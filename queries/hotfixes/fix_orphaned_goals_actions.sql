-- GET ACTIONS
SELECT f.data -> 'assessmentAndPlan' -> 'problemsGoalsActions' -> 'problemsGoalsActions'
FROM aspire.encounter e
       JOIN aspire.form f ON f.id = e.form_id
WHERE e.id = :encounter_id;

-- FIX ACTIONS
UPDATE aspire.form_data fd
SET data = jsonb_set(
  data,
  '{assessmentAndPlan,problemsGoalsActions,problemsGoalsActions,selectedActions}',
  $$
  FIXED_LIST_HERE
  $$
  )
FROM aspire.encounter e
WHERE e.form_id = fd.form_id
AND e.id = :id;
