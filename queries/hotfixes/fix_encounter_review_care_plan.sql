-- Set blanked out care plan to match encounter
UPDATE
  aspire.form_data er_f
SET
  data = jsonb_set(er_f.data, '{assessmentAndPlan,problemsGoalsActions}', e_f.data -> 'assessmentAndPlan' -> 'problemsGoalsActions')
FROM
  aspire.encounter_review er
  JOIN aspire.encounter e ON e.id = er.encounter_id
  JOIN aspire.form_data e_f ON e_f.form_id = e.form_id
WHERE
  er_f.form_id = er.form_id
  AND er.signed_at IS NOT NULL
  AND er_f.data -> 'assessmentAndPlan' -> 'problemsGoalsActions' -> 'problems' IS NULL;

