-- SELECT PROBLEMS LIST
SELECT fd.data->'assessmentAndPlan'->'problemsGoalsActions'->'problems'
FROM aspire.encounter e
JOIN aspire.form_data fd ON fd.form_id = e.form_id
WHERE e.id = :id;

-- FIX PROBLEMS LIST
UPDATE aspire.form_data fd
SET data = jsonb_set(
  data,
  '{assessmentAndPlan,problemsGoalsActions,problems}',
  $$
  FIXED_LIST_HERE
  $$
  )
FROM aspire.encounter e
WHERE e.form_id = fd.form_id
AND e.id = :id;
