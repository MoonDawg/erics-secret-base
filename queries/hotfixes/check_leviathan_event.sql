SELECT *
FROM events
WHERE event_type = 'Elixir.Leviathan.Events.AssessmentSigned'
  AND data = '{"id": ENCOUNTER_ID}';
