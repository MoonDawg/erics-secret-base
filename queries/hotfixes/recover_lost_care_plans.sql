WITH care_plan AS (
  SELECT
      (data->'assessmentAndPlan'->'problemsGoalsActions'->'selectedProblems') AS selected_problems,
      (data->'assessmentAndPlan'->'problemsGoalsActions'->'selectedGoals') AS selected_goals,
      (data->'assessmentAndPlan'->'problemsGoalsActions'->'selectedActions') AS selected_actions,
      (data->'assessmentAndPlan'->'problemsGoalsActions'->'assessmentNotes') AS assessment_notes
  FROM aspire.form_base
  WHERE id=:id
    AND (data->'assessmentAndPlan'->'problemsGoalsActions'->'selectedProblems') IS NOT NULL
    AND (data->'assessmentAndPlan'->'problemsGoalsActions'->'selectedProblems') <> '[]'
  ORDER BY (meta)."from" DESC
  LIMIT 1
),
problems AS (
  SELECT (data->'assessmentAndPlan'->'problemsGoalsActions'->'problems') AS problems
  FROM aspire.form
  WHERE id=:id
)
UPDATE aspire.form
  SET data = jsonb_set(
    data,
    '{assessmentAndPlan,problemsGoalsActions}',
    jsonb_build_object(
      'problems', (SELECT problems FROM problems),
      'selectedProblems', (SELECT selected_problems FROM care_plan),
      'selectedGoals', (SELECT selected_goals FROM care_plan),
      'selectedActions', (SELECT selected_actions FROM care_plan),
      'assessmentNotes', (SELECT assessment_notes FROM care_plan)
    )
  )
WHERE id=:id;
