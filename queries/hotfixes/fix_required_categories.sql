UPDATE
  aspire.form_context fd
SET
  context = jsonb_set(context, '{requiredRiskAdjustmentCategories}', $$ LIST GOES HERE $$)
WHERE
  fd.form_id = :form_id;

