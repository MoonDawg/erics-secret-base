 WITH date_range AS (
         SELECT date_trunc('week'::text, (now()::date - 30)::timestamp with time zone)::date AS first_date,
            now()::date AS last_date
        ), date_series AS (
         SELECT generate_series(dr.first_date::timestamp with time zone, dr.last_date::timestamp with time zone, '1 day'::interval)::date AS date
           FROM date_range dr
        ), patient_pop AS (
         SELECT ps.patient_id,
            p.np_care_team_id,
            p.status,
            p.substatus
           FROM entity.v_patient_service_info ps
             JOIN entity.v_patient p ON p.patient_id::text = ps.patient_id::text
          WHERE ps.program_enrolled::text ~* 'five percent'::text AND (ps.discharge_reason::text <> 'Gainshare Exclusion'::text OR ps.discharge_reason IS NULL)
        ), pre_agents AS (
         SELECT DISTINCT ct_1.id,
            app.market,
            ct_1.app AS agent,
            ct_1.app_id AS agent_id,
            'APP'::text AS role
           FROM entity.v_care_team ct_1
             LEFT JOIN entity.v_user_all app ON app.user_id::text = ct_1.app_id
          WHERE ct_1.type = 'five_percent'::text
        UNION
         SELECT DISTINCT ct_1.id,
            app.market,
            ct_1.field_social_worker AS agent,
            ct_1.field_social_worker_id AS agent_id,
            'Field SW'::text AS role
           FROM entity.v_care_team ct_1
             LEFT JOIN entity.v_user_all app ON app.user_id::text = ct_1.app_id
          WHERE ct_1.type = 'five_percent'::text
        UNION
         SELECT DISTINCT ct_1.id,
            app.market,
            COALESCE(ct_1.field_rn, ct_1.pcc_rn) AS agent,
            COALESCE(ct_1.field_rn_id, ct_1.pcc_rn_id) AS agent_id,
            'Field RN'::text AS role
           FROM entity.v_care_team ct_1
             LEFT JOIN entity.v_user_all app ON app.user_id::text = ct_1.app_id
          WHERE ct_1.type = 'five_percent'::text
        UNION
         SELECT DISTINCT ct_1.id,
            app.market,
            ct_1.physician AS agent,
            ct_1.physician_id AS agent_id,
            'Physician'::text AS role
           FROM entity.v_care_team ct_1
             LEFT JOIN entity.v_user_all app ON app.user_id::text = ct_1.app_id
          WHERE ct_1.type = 'five_percent'::text
        UNION
         SELECT DISTINCT ct_1.id,
            app.market,
            ct_1.nurse_support_specialist AS agent,
            ct_1.nurse_support_specialist_id AS agent_id,
            'NSS'::text AS role
           FROM entity.v_care_team ct_1
             LEFT JOIN entity.v_user_all app ON app.user_id::text = ct_1.app_id
          WHERE ct_1.type = 'five_percent'::text
        UNION
         SELECT DISTINCT ct_1.id,
            app.market,
            ct_1.css AS agent,
            ct_1.css_id AS agent_id,
            'CSS'::text AS role
           FROM entity.v_care_team ct_1
             LEFT JOIN entity.v_user_all app ON app.user_id::text = ct_1.app_id
          WHERE ct_1.type = 'five_percent'::text
        UNION
         SELECT DISTINCT ct_1.id,
            app.market,
            ct_1.hospice_nurse_support_specialist AS agent,
            ct_1.hospice_nurse_support_specialist_id AS agent_id,
            'Hospice NSS'::text AS role
           FROM entity.v_care_team ct_1
             LEFT JOIN entity.v_user_all app ON app.user_id::text = ct_1.app_id
          WHERE ct_1.type = 'five_percent'::text
        UNION
         SELECT DISTINCT ct_1.id,
            app.market,
            ct_1.hospice_rn AS agent,
            ct_1.hospice_rn_id AS agent_id,
            'Hospice RN'::text AS role
           FROM entity.v_care_team ct_1
             LEFT JOIN entity.v_user_all app ON app.user_id::text = ct_1.app_id
          WHERE ct_1.type = 'five_percent'::text
        UNION
         SELECT DISTINCT ct_1.id,
            app.market,
            ct_1.pes AS agent,
            ct_1.pes_id AS agent_id,
            'PES'::text AS role
           FROM entity.v_care_team ct_1
             LEFT JOIN entity.v_user_all app ON app.user_id::text = ct_1.app_id
          WHERE ct_1.type = 'five_percent'::text
        UNION
         SELECT DISTINCT ct_1.id,
            app.market,
            ct_1.chw AS agent,
            ct_1.chw_id AS agent_id,
            'CHW'::text AS role
           FROM entity.v_care_team ct_1
             LEFT JOIN entity.v_user_all app ON app.user_id::text = ct_1.app_id
          WHERE ct_1.type = 'five_percent'::text
        ), agent_ct_mkt_date_cartesian AS (
         SELECT DISTINCT ds.date,
            a.id,
            a.market,
            a.agent,
            a.agent_id,
            a.role
           FROM pre_agents a
             JOIN date_series ds ON true
          WHERE a.agent IS NOT NULL
        ), distinct_reach_agents AS (
         SELECT DISTINCT a.id AS care_team_id,
            a.agent,
            a.agent_id,
            a.role,
            u.aspire_role
           FROM pre_agents a
             LEFT JOIN entity.v_user_all u ON u.user_id::text = a.agent_id
          WHERE a.agent IS NOT NULL
        ), completed_visit_pop AS (
         SELECT ds.date,
            ra.agent_id,
            ra.care_team_id,
            count(cv_1.*) FILTER (WHERE cv_1.assessment_status = ANY (ARRAY['closed'::text, 'pending_md_review'::text])) AS visits_completed,
            count(cv_1.*) FILTER (WHERE (cv_1.assessment_status = ANY (ARRAY['closed'::text, 'pending_md_review'::text])) AND (v.location = ANY (ARRAY['remote'::text, 'Remote'::text, 'telephonic'::text, 'Telephonic'::text]))) AS remote_assessments_subset_completed,
            count(cv_1.*) FILTER (WHERE cv_1.assessment_status = 'cancelled'::text) AS cancelled_visits,
            count(cv_1.*) FILTER (WHERE cv_1.assessment_status = ANY (ARRAY['created'::text, 'in_progress'::text])) AS open_assessments,
            count(cv_1.*) FILTER (WHERE cv_1.type ~* 'healthy_start'::text AND (cv_1.assessment_status = ANY (ARRAY['closed'::text, 'pending_md_review'::text]))) AS healthy_starts_completed,
            count(cv_1.*) FILTER (WHERE cv_1.type ~* '_toc|toc_'::text AND (cv_1.assessment_status = ANY (ARRAY['closed'::text, 'pending_md_review'::text]))) AS toc_visits_completed,
            count(cv_1.*) FILTER (WHERE cv_1.type ~* 'urgent'::text AND (cv_1.assessment_status = ANY (ARRAY['closed'::text, 'pending_md_review'::text]))) AS urgent_visits_completed
           FROM date_series ds
             JOIN entity.v_aspire_encounter cv_1 ON cv_1.date_of_service = ds.date
             LEFT JOIN entity.v_aspire_visit v ON v.encounter_id = cv_1.id
             JOIN patient_pop p ON p.patient_id::text = cv_1.patient_id
             JOIN distinct_reach_agents ra ON ra.agent_id = cv_1.provider_id AND ra.care_team_id::text = p.np_care_team_id::text
          GROUP BY ds.date, ra.agent_id, ra.care_team_id
        ), scheduled_visit_pop AS (
         SELECT ds.date,
            ra.agent_id,
            ra.care_team_id,
            count(cv_1.*) AS visits_scheduled,
            count(cv_1.*) FILTER (WHERE cv_1.type ~* 'healthy_start'::text) AS healthy_starts_scheduled,
            count(cv_1.*) FILTER (WHERE cv_1.type ~* '_toc|toc_'::text) AS toc_visits_scheduled,
            count(cv_1.*) FILTER (WHERE cv_1.type ~* 'urgent'::text) AS urgent_visits_scheduled
           FROM date_series ds
             JOIN entity.v_aspire_encounter cv_1 ON cv_1.date_of_service = ds.date
             JOIN patient_pop p ON p.patient_id::text = cv_1.patient_id
             JOIN distinct_reach_agents ra ON ra.agent_id = cv_1.created_by AND ra.care_team_id::text = p.np_care_team_id::text
          GROUP BY ds.date, ra.agent_id, ra.care_team_id
        ), completed_tasks_pop AS (
         SELECT ds.date,
            ra.agent_id,
            ra.care_team_id,
            count(t.*) AS tasks_completed,
            count(t.*) FILTER (WHERE t.title ~ 'TOC'::text OR t.title ~* 'transition of care|transition_of_care'::text) AS toc_tasks_completed
           FROM date_series ds
             JOIN entity.v_aspire_task t ON COALESCE(t.completed_on::date, t.created_at::date) = ds.date AND t.status = 'completed'::text
             JOIN patient_pop p ON p.patient_id::text = t.patient_id
             JOIN distinct_reach_agents ra ON ra.agent_id = t.owner_id AND ra.care_team_id::text = p.np_care_team_id::text
          GROUP BY ds.date, ra.agent_id, ra.care_team_id
        ), non_completed_tasks_pop AS (
         SELECT ds.date,
            ra.agent_id,
            ra.care_team_id,
            count(t.*) AS tasks_assigned,
            count(t.*) FILTER (WHERE t.status <> ALL (ARRAY['completed'::text, 'cancelled'::text])) AS tasks_still_open,
            count(t.*) FILTER (WHERE (t.status <> ALL (ARRAY['completed'::text, 'cancelled'::text])) AND now()::date > t.due_on) AS tasks_open_overdue,
            count(t.*) FILTER (WHERE t.title ~ 'TOC'::text OR t.title ~* 'transition of care|transition_of_care'::text) AS toc_tasks_assigned,
            count(t.*) FILTER (WHERE (t.status <> ALL (ARRAY['completed'::text, 'cancelled'::text])) AND (t.title ~ 'TOC'::text OR t.title ~* 'transition of care|transition_of_care'::text)) AS toc_tasks_still_open,
            count(t.*) FILTER (WHERE (t.status <> ALL (ARRAY['completed'::text, 'cancelled'::text])) AND now()::date > t.due_on AND (t.title ~ 'TOC'::text OR t.title ~* 'transition of care|transition_of_care'::text)) AS toc_tasks_open_overdue
           FROM date_series ds
             JOIN entity.v_aspire_task t ON t.created_at::date = ds.date
             JOIN patient_pop p ON p.patient_id::text = t.patient_id
             JOIN distinct_reach_agents ra ON ra.agent_id = t.owner_id AND ra.care_team_id::text = p.np_care_team_id::text
          GROUP BY ds.date, ra.agent_id, ra.care_team_id
        ), all_orders_pop AS (
         SELECT ds.date,
            ra.agent_id,
            ra.care_team_id,
            count(o.*) AS orders_assigned,
            count(o.*) FILTER (WHERE ((o.status || '-'::text) || o.substatus) <> 'closed-success'::text AND (o.status <> ALL (ARRAY['cancelled'::text, 'declined'::text]))) AS orders_open,
            count(o.*) FILTER (WHERE ((o.status || '-'::text) || o.substatus) <> 'closed-success'::text AND (o.status <> ALL (ARRAY['cancelled'::text, 'declined'::text])) AND now()::date > o.due_date) AS orders_open_overdue
           FROM date_series ds
             JOIN entity.v_clinical_orders o ON o.created_at::date = ds.date
             JOIN patient_pop p ON p.patient_id::text = o.patient_id
             JOIN distinct_reach_agents ra ON ra.agent_id = o.currently_assigned_to AND ra.care_team_id::text = p.np_care_team_id::text
          GROUP BY ds.date, ra.agent_id, ra.care_team_id
        ), fulfilled_orders_pop AS (
         SELECT ds.date,
            ra.agent_id,
            ra.care_team_id,
            count(o.*) AS orders_fulfilled
           FROM date_series ds
             JOIN entity.v_clinical_orders o ON o.fulfillment_date = ds.date AND o.status = 'closed'::text AND o.substatus = 'success'::text
             JOIN patient_pop p ON p.patient_id::text = o.patient_id
             JOIN distinct_reach_agents ra ON ra.agent_id = o.fulfilled_by AND ra.care_team_id::text = p.np_care_team_id::text
          GROUP BY ds.date, ra.agent_id, ra.care_team_id
        ), call_counts AS (
         SELECT ds.date,
            ra.agent_id,
            ra.care_team_id,
            count(pc.*) AS call_count,
            count(pc.*) FILTER (WHERE (pc.disposition::text <> ALL (ARRAY['No Answer'::character varying::text, 'Left Message'::character varying::text, 'Busy'::character varying::text, 'Number Disconnected'::character varying::text, 'Wrong Number'::character varying::text])) OR pc.disposition IS NULL) AS connected_call_count
           FROM date_series ds
             JOIN entity.v_patient_calls pc ON pc.created_datetime::date = ds.date
             JOIN patient_pop p ON p.patient_id::text = pc.patient_id::text
             JOIN distinct_reach_agents ra ON ra.agent_id = pc.created_by::text AND ra.care_team_id::text = p.np_care_team_id::text
          GROUP BY ds.date, ra.agent_id, ra.care_team_id
        ), non_completed_episodes_of_care AS (
         SELECT ds.date,
            ra.agent_id,
            ra.care_team_id,
            count(eoc.*) AS eocs_assigned,
            count(eoc.*) FILTER (WHERE eoc.status <> ALL (ARRAY['cancelled'::text, 'closed'::text])) AS eocs_still_open
           FROM date_series ds
             JOIN entity.v_aspire_episode_of_care eoc ON eoc.created_at::date = ds.date
             JOIN patient_pop p ON p.patient_id::text = eoc.patient_id
             JOIN distinct_reach_agents ra ON ra.agent_id = eoc.owner_id AND ra.care_team_id::text = p.np_care_team_id::text
          GROUP BY ds.date, ra.agent_id, ra.care_team_id
        ), completed_episodes_of_care AS (
         SELECT ds.date,
            ra.agent_id,
            ra.care_team_id,
            count(eoc.*) AS eocs_closed
           FROM date_series ds
             JOIN entity.v_aspire_episode_of_care eoc ON eoc.completed_at::date = ds.date
             JOIN patient_pop p ON p.patient_id::text = eoc.patient_id
             JOIN distinct_reach_agents ra ON ra.agent_id = COALESCE(eoc.completed_by, eoc.owner_id) AND ra.care_team_id::text = p.np_care_team_id::text
          WHERE eoc.status = 'closed'::text
          GROUP BY ds.date, ra.agent_id, ra.care_team_id
        ), snapshot_patient_data AS (
         SELECT now()::date AS today,
            pa.agent_id,
            pa.id AS care_team_id,
            count(DISTINCT p.patient_id) FILTER (WHERE p.substatus::text ~* 'Unengaged'::text) AS unengaged_pts,
            count(DISTINCT p.patient_id) FILTER (WHERE p.status::text = 'Active'::text) AS active_pts,
            count(DISTINCT p.patient_id) FILTER (WHERE p.status::text = 'Active'::text AND now()::date > pv.next_targeted_visit_date) AS active_overdue_pts,
            count(DISTINCT p.patient_id) FILTER (WHERE p.status::text = 'Scheduled'::text) AS scheduled_pts,
            count(DISTINCT p.patient_id) FILTER (WHERE p.status::text = 'Referred'::text) AS referred_pts,
            count(DISTINCT p.patient_id) FILTER (WHERE p.status::text <> 'Discharged'::text) AS eligible_aka_non_discharged_pts
           FROM pre_agents pa
             JOIN patient_pop p ON p.np_care_team_id::text = pa.id::text
             LEFT JOIN entity.v_patient_visit_info pv ON pv.patient_id::text = p.patient_id::text
          GROUP BY (now()::date), pa.agent_id, pa.id
        )
 SELECT act.date,
    date_trunc('week'::text, act.date::timestamp with time zone)::date AS week_start,
    act.id AS care_team_id,
    ct.physician,
    rl.region,
    act.market,
    act.agent,
    act.agent_id,
    act.role,
    COALESCE(cv.visits_completed, 0::bigint) AS visits_completed,
    COALESCE(cv.remote_assessments_subset_completed, 0::bigint) AS remote_assessments_subset_completed,
    COALESCE(cv.cancelled_visits, 0::bigint) AS cancelled_visits,
    COALESCE(cv.open_assessments, 0::bigint) AS open_assessments,
    COALESCE(cv.healthy_starts_completed, 0::bigint) AS healthy_starts_completed,
    COALESCE(cv.toc_visits_completed, 0::bigint) AS toc_visits_completed,
    COALESCE(cv.urgent_visits_completed, 0::bigint) AS urgent_visits_completed,
    COALESCE(sv.visits_scheduled, 0::bigint) AS visits_scheduled,
    COALESCE(sv.healthy_starts_scheduled, 0::bigint) AS healthy_starts_scheduled,
    COALESCE(sv.toc_visits_scheduled, 0::bigint) AS toc_visits_scheduled,
    COALESCE(sv.urgent_visits_scheduled, 0::bigint) AS urgent_visits_scheduled,
    COALESCE(ctp.tasks_completed, 0::bigint) AS tasks_completed,
    COALESCE(ctp.toc_tasks_completed, 0::bigint) AS toc_tasks_completed,
    COALESCE(nct.tasks_assigned, 0::bigint) AS tasks_assigned,
    COALESCE(nct.tasks_still_open, 0::bigint) AS tasks_still_open,
    COALESCE(nct.tasks_open_overdue, 0::bigint) AS tasks_open_overdue,
    COALESCE(nct.toc_tasks_assigned, 0::bigint) AS toc_tasks_assigned,
    COALESCE(nct.toc_tasks_still_open, 0::bigint) AS toc_tasks_still_open,
    COALESCE(nct.toc_tasks_open_overdue, 0::bigint) AS toc_tasks_open_overdue,
    COALESCE(ao.orders_assigned, 0::bigint) AS orders_assigned,
    COALESCE(ao.orders_open, 0::bigint) AS orders_open,
    COALESCE(ao.orders_open_overdue, 0::bigint) AS orders_open_overdue,
    COALESCE(fo.orders_fulfilled, 0::bigint) AS orders_fulfilled,
    COALESCE(cc.call_count, 0::bigint) AS call_count,
    COALESCE(cc.connected_call_count, 0::bigint) AS connected_call_count,
    COALESCE(neoc.eocs_assigned, 0::bigint) AS eocs_assigned,
    COALESCE(neoc.eocs_still_open, 0::bigint) AS eocs_still_open,
    COALESCE(ceoc.eocs_closed, 0::bigint) AS eocs_closed,
    sp.unengaged_pts,
    sp.active_pts,
    sp.active_overdue_pts,
    sp.scheduled_pts,
    sp.referred_pts,
    sp.eligible_aka_non_discharged_pts
   FROM agent_ct_mkt_date_cartesian act
     LEFT JOIN entity.v_care_team ct ON ct.id::text = act.id::text
     LEFT JOIN reporting.region_lookup rl ON rl.market::text = act.market::text
     LEFT JOIN completed_visit_pop cv ON cv.agent_id = act.agent_id AND cv.date = act.date AND cv.care_team_id::text = act.id::text
     LEFT JOIN scheduled_visit_pop sv ON sv.agent_id = act.agent_id AND sv.date = act.date AND sv.care_team_id::text = act.id::text
     LEFT JOIN completed_tasks_pop ctp ON ctp.agent_id = act.agent_id AND ctp.date = act.date AND ctp.care_team_id::text = act.id::text
     LEFT JOIN non_completed_tasks_pop nct ON nct.agent_id = act.agent_id AND nct.date = act.date AND nct.care_team_id::text = act.id::text
     LEFT JOIN all_orders_pop ao ON ao.agent_id = act.agent_id AND ao.date = act.date AND ao.care_team_id::text = act.id::text
     LEFT JOIN fulfilled_orders_pop fo ON fo.agent_id = act.agent_id AND fo.date = act.date AND fo.care_team_id::text = act.id::text
     LEFT JOIN call_counts cc ON cc.agent_id = act.agent_id AND cc.date = act.date AND cc.care_team_id::text = act.id::text
     LEFT JOIN non_completed_episodes_of_care neoc ON neoc.agent_id = act.agent_id AND neoc.date = act.date AND neoc.care_team_id::text = act.id::text
     LEFT JOIN completed_episodes_of_care ceoc ON ceoc.agent_id = act.agent_id AND ceoc.date = act.date AND ceoc.care_team_id::text = act.id::text
     LEFT JOIN snapshot_patient_data sp ON sp.agent_id = act.agent_id AND sp.today = act.date AND sp.care_team_id::text = act.id::text;
