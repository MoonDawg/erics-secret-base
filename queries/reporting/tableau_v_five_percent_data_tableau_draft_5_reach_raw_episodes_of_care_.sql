 WITH date_range AS (
         SELECT GREATEST('2020-01-01'::date, now()::date - 90) AS first_date
        )
 SELECT eoc.id AS episode_id,
    eoc.patient_id,
    ('https://aspirehealth.io/patient-record/'::text || eoc.patient_id) || '/patient-info'::text AS emr_url,
    eoc.type,
    eoc.status AS episode_status,
    eoc.status_note AS episode_status_note,
    eoc.status_reason AS episode_status_reason,
    eoc.referral_source AS episode_referral_source,
    eoc.objective,
    eoc.next_targeted_outreach,
    (eoc.created_at - '06:00:00'::interval)::date AS created_date,
    (eoc.modified_at - '06:00:00'::interval)::date AS last_modified_date,
    (eoc.cancelled_at - '06:00:00'::interval)::date AS cancelled_date,
    (eoc.completed_at - '06:00:00'::interval)::date AS completed_date,
    assignee.name AS assignee,
    assignee.aspire_role AS assignee_role,
    requester.name AS requester,
    requester.aspire_role AS requester_role,
    creator.name AS creator,
    creator.aspire_role AS creator_role,
    completer.name AS completed_by,
    completer.aspire_role AS completed_by_role,
    canceller.name AS cancelled_by,
    canceller.aspire_role AS cancelled_by_role,
    last_mod.name AS last_mod_by,
    last_mod.aspire_role AS last_mod_by_role,
    ps.program_enrolled,
    p.full_name AS patient,
    p.date_of_birth AS dob,
    p.status,
    p.substatus,
    p.internal_substatus,
    p.market,
    rl.region,
    ct.app,
    ct.physician AS md,
    ct.clinical_manager AS cd,
    ps.initial_active_date
   FROM entity.v_aspire_episode_of_care eoc
     LEFT JOIN entity.v_patient_service_info ps ON ps.patient_id::text = eoc.patient_id
     LEFT JOIN entity.v_patient p ON p.patient_id::text = eoc.patient_id
     LEFT JOIN entity.v_care_team ct ON ct.id::text = p.np_care_team_id::text
     LEFT JOIN reporting.region_lookup rl ON rl.market::text = p.market::text
     LEFT JOIN entity.v_user_all assignee ON assignee.user_id::text = eoc.owner_id
     LEFT JOIN entity.v_user_all requester ON requester.user_id::text = eoc.requested_by
     LEFT JOIN entity.v_user_all creator ON creator.user_id::text = eoc.created_by
     LEFT JOIN entity.v_user_all completer ON completer.user_id::text = eoc.completed_by
     LEFT JOIN entity.v_user_all canceller ON canceller.user_id::text = eoc.cancelled_by
     LEFT JOIN entity.v_user_all last_mod ON last_mod.user_id::text = eoc.modified_by::text
     JOIN date_range dr ON true
  WHERE COALESCE((eoc.completed_at - '06:00:00'::interval)::date, (eoc.cancelled_at - '06:00:00'::interval)::date, (eoc.modified_at - '06:00:00'::interval)::date, (eoc.created_at - '06:00:00'::interval)::date) >= dr.first_date;
