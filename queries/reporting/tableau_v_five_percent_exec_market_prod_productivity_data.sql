 WITH date_range AS (
         SELECT '2020-01-02'::date AS first_date,
            now()::date AS last_date
        ), patient_pop AS (
         SELECT ps_1.patient_id,
            ps_1.program_enrolled
           FROM entity.v_patient_service_info ps_1
          WHERE ps_1.program_enrolled::text ~* 'five percent'::text
        ), relevant_task_list AS (
         SELECT t_1.patient_id,
            assignee.name AS assignee,
            assignee.user_id AS assignee_id,
            assignee.aspire_role AS assignee_role,
            originator.name AS originator,
            originator.aspire_role AS originator_role,
            completed.name AS completed_by,
            completed.aspire_role AS completed_by_role,
            canceller.name AS cancelled_by,
            canceller.aspire_role AS cancelled_by_role,
            t_1.title AS task_type,
            t_1.id AS task_id,
            t_1.status AS task_status,
            t_1.description,
            t_1.created_at::date AS created_date,
            t_1.completed_on::date AS completed_date,
            t_1.modified_at::date AS last_modified_date,
            t_1.cancellation_reason,
            t_1.cancelled_at::date AS cancelled_date,
            t_1.due_on AS due_date,
                CASE
                    WHEN (t_1.status <> ALL (ARRAY['completed'::text, 'cancelled'::text])) AND ('now'::text::date - t_1.due_on) = 1 THEN '1 Day Overdue'::text
                    WHEN (t_1.status <> ALL (ARRAY['completed'::text, 'cancelled'::text])) AND ('now'::text::date - t_1.due_on) >= 2 AND ('now'::text::date - t_1.due_on) <= 3 THEN '2-3 Days Overdue'::text
                    WHEN (t_1.status <> ALL (ARRAY['completed'::text, 'cancelled'::text])) AND ('now'::text::date - t_1.due_on) >= 4 AND ('now'::text::date - t_1.due_on) <= 7 THEN '4-7 Days Overdue'::text
                    WHEN (t_1.status <> ALL (ARRAY['completed'::text, 'cancelled'::text])) AND ('now'::text::date - t_1.due_on) >= 8 THEN '8+ Days Overdue'::text
                    ELSE 'Not Overdue'::text
                END AS aged
           FROM entity.v_aspire_task t_1
             LEFT JOIN entity.v_user_all assignee ON t_1.owner_id = assignee.user_id::text
             LEFT JOIN entity.v_user_all originator ON t_1.requested_by = originator.user_id::text
             LEFT JOIN entity.v_user_all completed ON t_1.completed_by = completed.user_id::text
             LEFT JOIN entity.v_user_all canceller ON t_1.cancelled_by = canceller.user_id::text
             JOIN entity.v_patient_service_info ps_1 ON ps_1.patient_id::text = t_1.patient_id AND ps_1.program_enrolled::text ~* 'five percent'::text
             JOIN date_range dr ON true
          WHERE COALESCE(t_1.completed_on::date, t_1.cancelled_at::date) >= dr.first_date OR (t_1.status <> ALL (ARRAY['completed'::text, 'cancelled'::text]))
        ), assessment_pop AS (
         SELECT e.id AS encounter_id,
            e.patient_id,
            e.type AS encounter_type,
            e.assessment_status AS case_status,
            e.created_at::date AS created_date,
            e.created_at,
            e.date_of_service,
            e.place_of_service,
            pes.name AS created_by,
            pes.aspire_role AS created_by_role,
            e.provider_id,
            COALESCE(v.location, 'no info (blank)'::text) AS location
           FROM entity.v_aspire_encounter e
             LEFT JOIN entity.v_aspire_visit v ON v.encounter_id = e.id
             JOIN patient_pop p_1 ON e.patient_id = p_1.patient_id::text
             LEFT JOIN entity.v_user_all pes ON pes.user_id::text = e.created_by
             JOIN date_range dr ON e.created_at::date >= dr.first_date AND e.created_at::date <= dr.last_date
        ), orders_pop AS (
         SELECT o_1.created_at::date AS createddate,
            date_trunc('week'::text, o_1.created_at::date::timestamp with time zone)::date AS week_created,
            now()::date - o_1.created_at::date AS order_age_days,
            o_1.id AS order_id,
                CASE
                    WHEN o_1.type ~* 'hospice'::text THEN 'Hospice'::text
                    ELSE 'Non-Hospice'::text
                END AS type_bucket,
            o_1.type,
            o_1.subtype,
            o_1.status,
            o_1.substatus,
            o_1.aspire_submit_date,
            date_trunc('week'::text, o_1.aspire_submit_date::timestamp with time zone)::date AS week_submitted,
                CASE
                    WHEN o_1.aspire_submit_date IS NULL THEN NULL::bigint
                    ELSE (( SELECT count(*) AS count
                       FROM generate_series(0, o_1.aspire_submit_date - o_1.created_at::date) i(i)
                      WHERE date_part('dow'::text, o_1.created_at::date + i.i) <> ALL (ARRAY[0::double precision, 6::double precision]))) - 1
                END AS workdays_to_submit,
                CASE
                    WHEN o_1.aspire_submit_date IS NULL THEN NULL::integer
                    ELSE o_1.aspire_submit_date - o_1.created_at::date
                END AS days_to_submit,
            now()::date - o_1.aspire_submit_date AS days_since_submitted,
                CASE
                    WHEN o_1.aspire_submit_date <= now()::date THEN 1
                    ELSE 0
                END AS submitted,
                CASE
                    WHEN o_1.status = 'closed'::text AND o_1.substatus = 'success'::text THEN 'Fulfilled'::text
                    WHEN (o_1.status = ANY (ARRAY['cancelled'::text, 'declined'::text])) OR o_1.substatus = 'patient_declined'::text THEN 'Cancelled'::text
                    WHEN o_1.status = 'closed'::text THEN 'Closed - Not Bucketed'::text
                    WHEN o_1.status = 'new'::text THEN 'Open - New'::text
                    ELSE 'Open - In Process'::text
                END AS closed_status,
                CASE
                    WHEN o_1.status = 'closed'::text THEN 1
                    WHEN (o_1.status = ANY (ARRAY['cancelled'::text, 'declined'::text])) OR o_1.substatus = 'patient_declined'::text THEN 1
                    WHEN o_1.status = 'new'::text THEN 0
                    ELSE 0
                END AS closed_bin,
            ct.nurse_support_specialist AS nss_ct,
            ct.app,
            ct.physician AS md,
            ct.clinical_manager AS cd,
            o_1.assigned_to_name AS step_assignee,
            o_1.assigned_to_role AS assignee_role,
            o_1.created_by_name,
            o_1.created_by_role,
            'Order'::text AS recordtype,
            p.patient_id,
            p.full_name AS patient,
            ('https://aspirehealth.io/patient-record/'::text || p.patient_id::text) || '/patient-info'::text AS emr_url,
            p.date_of_birth AS dob,
            p.status AS pt_status,
            p.substatus AS pt_substatus,
            p.internal_substatus,
            ps.program_enrolled,
            rl.region,
            p.market,
            hp.healthplan,
            p.primary_contract,
            p.contact_id,
            date_trunc('week'::text, o_1.modified_at::date::timestamp with time zone)::date AS week_last_mod,
            o_1.modified_at::date AS last_mod_date,
            o_1.modified_by_name,
            o_1.modified_by_role,
            o_1.cancelled_by_name,
            o_1.cancelled_by_role,
            date_trunc('week'::text, o_1.cancelled_date::timestamp with time zone)::date AS week_cancelled,
            o_1.cancelled_date,
            o_1.cancellation_reason,
            o_1.note AS order_notes,
            a.name AS vendor,
            a.phone AS vendor_phone,
            o_1.fulfilled_by_name,
            o_1.fulfilled_by_role,
            o_1.expected_fulfillment_date,
            date_trunc('week'::text, o_1.fulfillment_date::timestamp with time zone)::date AS week_fulfilled,
            date_trunc('month'::text, o_1.fulfillment_date::timestamp with time zone)::date AS month_fulfilled,
            o_1.fulfillment_date,
                CASE
                    WHEN o_1.status = 'closed'::text AND (o_1.substatus = 'success'::text) IS NOT NULL THEN 1
                    ELSE 0
                END AS fulfilled,
                CASE
                    WHEN o_1.fulfillment_date IS NULL THEN NULL::bigint
                    ELSE (( SELECT count(*) AS count
                       FROM generate_series(0, o_1.fulfillment_date - o_1.aspire_submit_date) i(i)
                      WHERE date_part('dow'::text, o_1.aspire_submit_date + i.i) <> ALL (ARRAY[0::double precision, 6::double precision]))) - 1
                END AS workdays_to_vendor_completion
           FROM entity.v_clinical_orders o_1
             JOIN entity.v_patient p ON p.patient_id::text = o_1.patient_id
             JOIN entity.v_patient_service_info ps ON ps.patient_id::text = o_1.patient_id
             LEFT JOIN entity.v_care_team ct ON ct.id::text = p.np_care_team_id::text
             LEFT JOIN reporting.region_lookup rl ON rl.market::text = p.market::text
             LEFT JOIN entity.v_aspire_healthplan_crosswalk hp ON hp.primary_contract::text = p.primary_contract::text
             LEFT JOIN entity.v_salesforce_account a ON a.sfid::text = o_1.vendor_id
             JOIN date_range dr ON true
          WHERE ps.program_enrolled::text ~* 'five percent'::text AND ((o_1.status <> ALL (ARRAY['closed'::text, 'cancelled'::text, 'declined'::text])) AND o_1.substatus <> 'patient_declined'::text OR COALESCE(o_1.fulfillment_date, o_1.cancelled_date, o_1.modified_at::date) >= dr.first_date)
        )
 SELECT p.full_name AS patient,
    'Assessment'::text AS productivitytype,
    a.encounter_type AS subtype,
    a.case_status AS status,
    a.date_of_service AS date,
    prov.name AS provider,
    prov.aspire_role AS role,
    a.place_of_service,
    p.market,
    p.status AS patientstatus,
    ps.program_enrolled,
    p.patient_id
   FROM assessment_pop a
     LEFT JOIN entity.v_patient p ON p.patient_id::text = a.patient_id
     LEFT JOIN entity.v_patient_service_info ps ON ps.patient_id::text = a.patient_id
     LEFT JOIN reporting.region_lookup rl ON rl.market::text = p.market::text
     LEFT JOIN entity.v_user_all prov ON prov.user_id::text = a.provider_id
  WHERE a.encounter_type !~* 'palliative|telephonic|telehealth|home based|home_based'::text OR a.encounter_type ~* 'top_five'::text
UNION
 SELECT p.full_name AS patient,
    'Tasks'::text AS productivitytype,
    t.task_type AS subtype,
    t.task_status AS status,
    t.completed_date AS date,
    t.assignee AS provider,
    t.assignee_role AS role,
    'null'::text AS place_of_service,
    p.market,
    p.status AS patientstatus,
    ps.program_enrolled,
    p.patient_id
   FROM relevant_task_list t
     LEFT JOIN entity.v_patient p ON p.patient_id::text = t.patient_id
     LEFT JOIN entity.v_patient_service_info ps ON ps.patient_id::text = t.patient_id
     LEFT JOIN entity.v_patient_visit_info pv ON pv.patient_id::text = t.patient_id
     LEFT JOIN reporting.region_lookup rl ON rl.market::text = p.market::text
     LEFT JOIN entity.v_aspire_healthplan_crosswalk hp ON hp.primary_contract::text = p.primary_contract::text
     LEFT JOIN entity.v_user_all u1 ON u1.user_id::text = p.app_id::text
     LEFT JOIN entity.v_care_team ct ON ct.id::text = p.np_care_team_id::text
UNION
 SELECT p.full_name AS patient,
    'Calls'::text AS productivitytype,
    pc.disposition AS subtype,
    'Null'::text AS status,
    (pc.created_datetime - '06:00:00'::interval)::date AS date,
    pc.caller_name AS provider,
    pc.caller_role AS role,
    'null'::text AS place_of_service,
    p.market,
    p.status AS patientstatus,
    ps.program_enrolled,
    p.patient_id
   FROM entity.v_patient_calls pc
     JOIN date_range dr ON pc.created_datetime::date >= dr.first_date
     JOIN entity.v_patient_service_info ps ON ps.patient_id::text = pc.patient_id::text AND ps.program_enrolled::text ~* 'five percent'::text
     LEFT JOIN entity.v_patient p ON p.patient_id::text = pc.patient_id::text
     LEFT JOIN reporting.region_lookup rl ON rl.market::text = p.market::text
     LEFT JOIN entity.v_care_team ct ON ct.id::text = p.np_care_team_id::text
UNION
 SELECT o.patient,
    'Orders'::text AS productivitytype,
    o.type AS subtype,
    o.status,
    o.fulfillment_date AS date,
    o.fulfilled_by_name AS provider,
    o.fulfilled_by_role AS role,
    'null'::text AS place_of_service,
    o.market,
    o.pt_status AS patientstatus,
    o.program_enrolled,
    o.patient_id
   FROM orders_pop o
UNION
 SELECT p.full_name AS patient,
    'EOCs'::text AS productivitytype,
    eoc.type AS subtype,
    eoc.status,
    (eoc.completed_at - '06:00:00'::interval)::date AS date,
    assignee.name AS provider,
    assignee.aspire_role AS role,
    'null'::text AS place_of_service,
    p.market,
    p.status AS patientstatus,
    ps.program_enrolled,
    eoc.patient_id
   FROM entity.v_aspire_episode_of_care eoc
     LEFT JOIN entity.v_patient_service_info ps ON ps.patient_id::text = eoc.patient_id
     LEFT JOIN entity.v_patient p ON p.patient_id::text = eoc.patient_id
     LEFT JOIN entity.v_care_team ct ON ct.id::text = p.np_care_team_id::text
     LEFT JOIN reporting.region_lookup rl ON rl.market::text = p.market::text
     LEFT JOIN entity.v_user_all assignee ON assignee.user_id::text = eoc.owner_id
     LEFT JOIN entity.v_user_all requester ON requester.user_id::text = eoc.requested_by
     LEFT JOIN entity.v_user_all creator ON creator.user_id::text = eoc.created_by
     LEFT JOIN entity.v_user_all completer ON completer.user_id::text = eoc.completed_by
     LEFT JOIN entity.v_user_all canceller ON canceller.user_id::text = eoc.cancelled_by
     LEFT JOIN entity.v_user_all last_mod ON last_mod.user_id::text = eoc.modified_by::text
     JOIN date_range dr ON true
  WHERE COALESCE((eoc.completed_at - '06:00:00'::interval)::date, (eoc.cancelled_at - '06:00:00'::interval)::date, (eoc.modified_at - '06:00:00'::interval)::date, (eoc.created_at - '06:00:00'::interval)::date) >= dr.first_date;
