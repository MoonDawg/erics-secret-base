SELECT
  count(*) AS patient_count,
  hcc_per_patient_count,
  hp.market,
  hp.healthplan
FROM (
  SELECT
    count(*) AS hcc_per_patient_count,
    patient_id
  FROM
    aspire.patient_risk_adjustment ra
    JOIN aspire.hcc h ON h.code = ra.hcc_code
  WHERE
    h.include
  GROUP BY
    patient_id) patient_hccs
  JOIN salesforce.v_patients p ON p.sfid = patient_hccs.patient_id
  JOIN aspire.healthplan_crosswalk hp ON hp.primary_contract = p.primary_contract__c
GROUP BY
  hcc_per_patient_count,
  hp.market,
  hp.healthplan
ORDER BY
  hp.market,
  hp.healthplan ASC,
  patient_count DESC;

