-- CHECK MISSING EXTERNAL ASSESSMENT ID
SELECT
  e.id AS encounter_id,
  c.sfid AS case_id
FROM
  aspire.encounter e
  JOIN salesforce.event ev ON ev.sfid = e.event_id
  JOIN salesforce.case c ON c.sfid = ev.whatid
WHERE
  c.external_assessment_id__c IS NULL;

-- CHECK MISSING CARE PLANS

SELECT
  e.id,
  e.signed_at,
  e.assessment_status,
  c.date_of_service__c,
  c.assessment_of_patient_condition__c,
  c.patient_management_plan__c,
  c.md_assessment__c,
  c.md_patient_management_plan__c
FROM
  aspire.encounter e
  JOIN salesforce.case c ON c.external_assessment_id__c = e.id
WHERE
  e.type IN ('palliative_initial', 'palliative_follow_up')
  AND e.assessment_status IN ('closed', 'pending_md_review')
  AND (c.status = 'Assessment Signed'
    OR c.status = 'Closed')
  AND (c.assessment_of_patient_condition__c IS NULL
    OR c.patient_management_plan__c IS NULL
    OR c.md_assessment__c IS NULL
    OR c.md_patient_management_plan__c IS NULL
    OR c.md_assessment__c = 'null')
  AND c.date_of_service__c > '02-01-2019'
ORDER BY
  e.id ASC;

-- CHECK NON SIGNED CASES

SELECT
  e.id
FROM
  aspire.encounter e
  JOIN salesforce.case c ON c.external_assessment_id__c = e.id
WHERE
  e.assessment_status IN ('closed', 'pending_md_review')
  AND c.status = 'Scheduled';

