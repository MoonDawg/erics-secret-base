select
  pat.sfid as patient_id,
  pat.name as patient_name,
  coalesce(pt.label, p.custom_label) as problem_label,
  p.problem_type_id is null as is_custom_problem,
  coalesce(gt.label, g.custom_label) as goal_label,
  g.goal_type_id is null as is_custom_goal,
  a.supplement as custom_action_label
from salesforce.v_patients pat
join clinical.problem p on p.patient_id = pat.sfid
left join clinical.problem_type pt on pt.id = p.problem_type_id
join clinical.goal g on g.problem_id = p.id
left join clinical.goal_type gt on gt.id = g.goal_type_id
join clinical.action a on a.goal_id = g.id
where
  pat.program_enrolled__pc in (
    'Five Percent Tier One',
    'Five Percent Tier Three',
    'Five Percent Tier Two'
  )
  and a.action_type_id is null
order by
  pat.name,
  problem_label,
  goal_label,
  custom_action_label;
