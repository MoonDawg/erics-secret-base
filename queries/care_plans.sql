SELECT e.patient_id,
  e.id AS encounter_id,
  p.id AS problem_id,
  g.id AS goal_id,
  a.id AS action_id,
  pc.category AS problem_category,
  pc.subcategory AS problem_subcategory,
  COALESCE(pt.label, p.custom_label) AS problem_label,
  p.problem_type_id IS NULL AS is_custom_problem,
  COALESCE(gt.label, g.custom_label) AS goal_label,
  g.goal_type_id IS NULL AS is_custom_goal,
  CASE
    WHEN at.label_plan IS NOT NULL
    AND a.supplement IS NOT NULL THEN CONCAT(at.label_plan, ' ', a.supplement)
    WHEN at.label_plan IS NOT NULL THEN at.label_plan
    ELSE a.supplement
  END AS action_label,
  a.action_type_id IS NULL AS is_custom_action
FROM aspire.encounter e
  JOIN clinical.goal g ON g.encounter_id = e.id
  LEFT JOIN clinical.goal_type gt ON gt.id = g.goal_type_id
  JOIN clinical.action a ON a.encounter_id = e.id
  AND a.goal_id = g.id
  LEFT JOIN clinical.action_type at ON at.id = a.action_type_id
  JOIN clinical.problem p ON p.id = g.problem_id
  JOIN clinical.problem_category pc ON pc.id = p.problem_category_id
  LEFT JOIN clinical.problem_type pt ON pt.id = p.problem_type_id
WHERE (
    p.problem_type_id IS NULL
    OR g.goal_type_id IS NULL
    OR a.action_type_id IS NULL
  )
  AND p.created_at > :created_at
ORDER BY p.rank,
  g.id,
  a.id ASC;