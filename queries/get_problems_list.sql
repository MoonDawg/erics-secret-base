-- List
SELECT
u.name AS "APP Name",
a.name AS "Patient Name",
COALESCE(pt.label, p.custom_label) AS "Problem Label",
CASE WHEN p.problem_type_id IS NULL THEN 'Yes' ELSE 'No' END AS "Custom Problem",
p.notes AS "Problem Notes",
p.modified_at::DATE AS "Last Updated"
FROM clinical.problem p
JOIN salesforce.account a ON a.sfid = p.patient_id
JOIN salesforce.user u ON u.sfid = a.ownerid
LEFT JOIN clinical.problem_type pt ON pt.id = p.problem_type_id
ORDER BY u.name, a.name, pt.label;

-- Groupings
SELECT
pt.id AS "Problem Id",
pc.category AS "Category",
pc.subcategory AS "Subcategory",
COALESCE(pt.label, p.custom_label) AS "Problem Label",
p.custom_label IS NOT NULL AS "Custom Problem",
count(*) AS "Problem Count"
FROM clinical.problem p
LEFT JOIN clinical.problem_type pt ON pt.id = p.problem_type_id
LEFT JOIN clinical.problem_category pc ON pc.id = p.problem_category_id
GROUP BY pt.id, pt.label, p.custom_label, pc.category, pc.subcategory
ORDER BY count(*) DESC;
