-- -- Get list of encounters and encounter reviews for Tara
-- WITH encounter_reviews AS (
--   SELECT
--     e.id AS encounter_id,
--     ev.id AS encounter_review_id,
--     u.name AS physician_name
--   FROM aspire.encounter e
--   JOIN salesforce.v_patients p ON p.sfid = e.patient_id
--   JOIN aspire.healthplan_crosswalk hp ON hp.primary_contract = p.primary_contract__c
--   LEFT JOIN aspire.encounter_review ev ON ev.encounter_id = e.id
--   LEFT JOIN salesforce.user u ON u.sfid = ev.physician_id
--   WHERE
--     e.assessment_status = 'pending_md_review'
--     AND hp.market IN ('Chicago')
--     AND u.name = 'Tara Friedman'
-- ),
-- delete_encounter_reviews AS (
--   UPDATE aspire.encounter_review_base
--   SET
--     meta."to" = now()
--   WHERE
--     id IN (
--       SELECT
--         encounter_review_id
--       FROM encounter_reviews
--     )
--     AND (meta)."to" IS NULL
-- ),
-- close_encounters AS (
--   UPDATE aspire.encounter
--   SET
--     assessment_status = 'closed'
--   WHERE
--     id IN (
--       SELECT
--         encounter_id
--       FROM encounter_reviews
--     )
--     AND assessment_status = 'pending_md_review'
-- )
-- SELECT
--   *
-- FROM encounter_reviews;

-- Get list of encounters and encounter reviews for Ashlei
WITH encounter_reviews AS (
  SELECT
    e.id AS encounter_id,
    ev.id AS encounter_review_id,
    u.name AS physician_name
  FROM aspire.encounter e
  JOIN salesforce.v_patients p ON p.sfid = e.patient_id
  JOIN aspire.healthplan_crosswalk hp ON hp.primary_contract = p.primary_contract__c
  LEFT JOIN aspire.encounter_review ev ON ev.encounter_id = e.id
  LEFT JOIN salesforce.user u ON u.sfid = ev.physician_id
  WHERE
    e.assessment_status = 'pending_md_review'
    -- AND hp.market IN (
    --   'Evansville',
    --   'South Bend',
    --   'Grand Rapids',
    --   'Chicago'
    -- )
    AND e.provider_id IN (
      '0051C000009LyIdQAK', -- Christine Hale
      '0051C000005Nah9QAC', -- Deborah Dorsch
      '0051C000008b2uoQAA', -- Barbara Chupp-Grove
      '0051C000008cFWuQAM'  -- Necia Garrett
    )
    AND u.name = 'Ashlei Lowery'
),
delete_encounter_reviews AS (
  DELETE FROM aspire.encounter_review
  WHERE
    id IN (
      SELECT
        encounter_review_id
      FROM encounter_reviews
    )
),
close_encounters AS (
  UPDATE aspire.encounter
  SET
    assessment_status = 'closed'
  WHERE
    id IN (
      SELECT
        encounter_id
      FROM encounter_reviews
    )
    AND assessment_status = 'pending_md_review'
)
SELECT
  *
FROM encounter_reviews;
