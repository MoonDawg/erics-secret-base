UPDATE salesforce.case SET external_assessment_id__c = null WHERE external_assessment_id__c = :encounter_id;
UPDATE aspire.encounter_base SET meta."to" = now() WHERE id = :encounter_id AND (meta)."to" IS NULL;

SELECT * FROM aspire.encounter WHERE id = :encounter_id;
