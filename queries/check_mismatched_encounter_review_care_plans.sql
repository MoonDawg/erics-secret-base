SELECT
  er.id,
  er.status,
  er_f.data -> 'assessmentAndPlan' -> 'problemsGoalsActions' -> 'problems',
  e_f.data -> 'assessmentAndPlan' -> 'problemsGoalsActions' -> 'problems'
FROM
  aspire.encounter_review er
  JOIN aspire.form er_f ON er_f.id = er.form_id
  JOIN aspire.encounter e ON e.id = er.encounter_id
  JOIN aspire.form e_f ON e_f.id = e.form_id
WHERE
  er_f.data -> 'assessmentAndPlan' -> 'problemsGoalsActions' -> 'problems' -> 0 -> 'id' <> e_f.data -> 'assessmentAndPlan' -> 'problemsGoalsActions' -> 'problems' -> 0 -> 'id';

