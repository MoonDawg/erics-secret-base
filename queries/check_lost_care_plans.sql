SELECT e.id, e.form_id, f.data->'assessmentAndPlan'->'problemsGoalsActions'->'selectedProblems' AS test FROM aspire.encounter e
JOIN aspire.form f ON f.id = e.form_id
WHERE (e.type='palliative_initial' OR e.type='palliative_follow_up')
  AND e.assessment_status='closed'
  AND (
    f.data->'assessmentAndPlan'->'problemsGoalsActions'->'selectedProblems' IS NULL OR
    f.data->'assessmentAndPlan'->'problemsGoalsActions'->'selectedProblems' = '[]'
  );
