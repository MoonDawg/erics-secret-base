-- Hospitalization Count
SELECT
  count(*)
FROM (
  SELECT
    p.patient_id
  FROM
    entity.v_patient p
    JOIN entity.v_patient_periods_of_service pps ON pps.patient_id = p.patient_id
    JOIN entity.v_hospitalization h ON h.patient_id = p.patient_id
      AND h.date_of_service__c BETWEEN pps.on_service
      AND pps.off_service) admits
WHERE
  patient_id = :id
GROUP BY
  patient_id;

-- Healthplan Tubes v1

SELECT
  hc.healthplan,
  sum((
      CASE WHEN (aspire.form_field_value (f.data, f.tags, 'abdomenEquipment') ? 'g_tube_ostomy'
          AND NOT aspire.form_field_value (f.data, f.tags, 'abdomenEquipment') ? 'j_tube_ostomy') THEN
        1
      ELSE
        0
      END)) g_tube_only,
  sum((
      CASE WHEN (aspire.form_field_value (f.data, f.tags, 'abdomenEquipment') ? 'j_tube_ostomy'
          AND NOT aspire.form_field_value (f.data, f.tags, 'abdomenEquipment') ? 'g_tube_ostomy') THEN
        1
      ELSE
        0
      END)) j_tube_only,
  sum((
      CASE WHEN (aspire.form_field_value (f.data, f.tags, 'abdomenEquipment') ? 'g_tube_ostomy'
          AND aspire.form_field_value (f.data, f.tags, 'abdomenEquipment') ? 'j_tube_ostomy') THEN
        1
      ELSE
        0
      END)) both_g_and_j_tube
FROM
  salesforce.v_patients p
  JOIN (
    SELECT
      e1.*
    FROM (
      SELECT
        *
      FROM
        aspire.encounter
      WHERE
        TYPE IN ('palliative_initial', 'palliative_follow_up', 'palliative_urgent')
        AND assessment_status IN ('closed', 'pending_md_review', 'pending_stakeholder')) e1
    LEFT JOIN (
      SELECT
        *
      FROM
        aspire.encounter
      WHERE
        TYPE IN ('palliative_initial', 'palliative_follow_up', 'palliative_urgent')
        AND assessment_status IN ('closed', 'pending_md_review', 'pending_stakeholder')) e2 ON e1.patient_id = e2.patient_id
      AND e1.signed_at < e2.signed_at
    WHERE
      e2.id IS NULL) e ON e.patient_id = p.sfid
  JOIN aspire.form f ON f.id = e.form_id
  JOIN aspire.healthplan_crosswalk hc ON hc.primary_contract = p.primary_contract__c
WHERE
  p.patient_status__pc = 'Active'
GROUP BY
  hc.healthplan
ORDER BY
  hc.healthplan ASC;

