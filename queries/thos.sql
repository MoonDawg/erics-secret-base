SELECT q.id,
  q.action,
  q.created_at,
  ce.start_at,
  q.patient_id,
  q.user_id,
  e.id,
  pce.email,
  pcp.phone,
  v.telehealth_os_appointment_id AS appointment_thos_id,
  a.telehealth_os_id AS patient_thos_id
FROM aspire.telehealth_os_queue q
  LEFT JOIN aspire.visit v ON v.id = q.visit_id
  LEFT JOIN aspire.calendar_event ce ON ce.id = v.calendar_event_id
  LEFT JOIN aspire.encounter e ON e.id = v.encounter_id
  LEFT JOIN salesforce.account a ON a.sfid = e.patient_id
  LEFT JOIN aspire.patient_contact pc ON pc.patient_id = a.sfid
  AND pc.preferred
  LEFT JOIN aspire.patient_contact_email pce ON pce.contact_id = pc.id
  AND pce.primary
  LEFT JOIN aspire.patient_contact_phone pcp ON pcp.contact_id = pc.id
  AND pcp.primary
WHERE NOT q.processed;