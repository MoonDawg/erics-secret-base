CREATE OR REPLACE VIEW entity.v_encounter_care_plan AS SELECT
  pat.sfid AS patient_id,
  hc.primary_contract,
  hc.market,
  hc.healthplan,
  ctp.name AS pod,
  e.provider_id,
  e.date_of_service,
  e.id AS encounter_id,
  p.id AS problem_id,
  g.id AS goal_id,
  a.id AS action_id,
  pc.subcategory AS problem_category,
  COALESCE(pt.label, p.custom_label) AS problem_label,
  p.problem_type_id IS NULL AS is_custom_problem,
  COALESCE(gt.label, g.custom_label) AS goal_label,
  g.goal_type_id IS NULL AS is_custom_goal,
  CASE
    WHEN at.label_plan IS NOT NULL
    AND a.supplement IS NOT NULL THEN CONCAT(at.label_plan, ' ', a.supplement)
    WHEN at.label_plan IS NOT NULL THEN at.label_plan
    ELSE a.supplement
  END AS action_label,
  a.action_type_id IS NULL AS is_custom_action
FROM aspire.encounter e
JOIN clinical.goal g ON g.encounter_id = e.id
LEFT JOIN clinical.goal_type gt ON gt.id = g.goal_type_id
JOIN clinical.action a ON a.encounter_id = e.id AND a.goal_id = g.id
LEFT JOIN clinical.action_type at ON at.id = a.action_type_id
JOIN clinical.problem p ON p.id = g.problem_id
JOIN clinical.problem_category pc ON pc.id = p.problem_category_id
LEFT JOIN clinical.problem_type pt ON pt.id = p.problem_type_id
JOIN salesforce.v_patients pat ON pat.sfid = e.patient_id
JOIN aspire.patient_care_team pct ON pct.patient_id = pat.sfid
JOIN aspire.care_team_care_team_pod ctctp ON ctctp.care_team_id = pct.care_team_id
JOIN aspire.care_team_pod ctp ON ctp.id = ctctp.pod_id
JOIN aspire.healthplan_crosswalk hc ON hc.primary_contract = pat.primary_contract__c
ORDER BY
  g.encounter_id,
  p.rank,
  g.id,
  a.id ASC;
