-- "Delete" Encounter Reviews
UPDATE
  aspire.encounter_review_base
SET
  meta. "to" = now()
WHERE
  id IN IDS_HERE
  AND (meta)."to" IS NULL;

-- Close Encounters
UPDATE
  aspire.encounter
SET
  assessment_status = 'closed'
WHERE
  id IN IDS_HERE
  AND assessment_status = 'pending_md_review';

