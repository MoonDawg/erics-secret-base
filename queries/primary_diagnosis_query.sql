SELECT
  a.sfid as patient_id,
  hc.healthplan,
  p1.modified_at as problem_created_date,
  pt1.label as problem_type_label_one,
  p1.custom_label as custom_problem_one,
  a.primary_diagnosis__pc,
  pt2.label as problem_type_label_two,
  p2.custom_label as custom_problem_two,
  a.secondary_diagnosis__pc
FROM salesforce.account a
JOIN clinical.problem p1 ON p1.patient_id = a.sfid
  AND p1.rank = 1
LEFT JOIN clinical.problem_type pt1 ON pt1.id = p1.problem_type_id
LEFT JOIN clinical.problem p2 ON p2.patient_id = a.sfid
  AND p2.rank = 2
LEFT JOIN clinical.problem_type pt2 ON pt2.id = p2.problem_type_id
JOIN aspire.healthplan_crosswalk hc ON a.primary_contract__c = hc.primary_contract
WHERE
  a.patient_status__pc = 'Active'
  AND a.primary_diagnosis__pc IS NULL;
