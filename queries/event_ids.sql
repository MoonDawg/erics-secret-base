SELECT
  e.sfid AS event_id,
  e.type,
  e.startdatetime,
  u.name AS app_name,
  cm.name AS clinical_manager
FROM salesforce.event e
JOIN salesforce.user u ON u.sfid = e.ownerid
JOIN salesforce.account a ON a.sfid = e.accountid
LEFT JOIN salesforce.care_team__c ct ON ct.nurse_practitioner_id__c = u.sfid
LEFT JOIN salesforce.user cm ON cm.sfid = ct.clinical_manager__c
WHERE
  e.accountid = :patient_id
  AND e.startdatetime :: DATE = :date_of_service;
