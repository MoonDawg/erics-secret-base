-- Create a random sfid for an encounter's case
UPDATE salesforce.case c
SET sfid = 'rand_' || (random() * 1000000000) :: INT :: TEXT where sfid is null;
