-- Custom problem care plans
SELECT g.encounter_id, pc.subcategory, p.custom_label as problem, g.custom_label as goal, a.supplement as action
FROM clinical.problem p
JOIN clinical.problem_category pc ON pc.id = p.problem_category_id
JOIN clinical.goal g ON g.problem_id = p.id
JOIN clinical.action a ON a.goal_id = g.id
JOIN aspire.encounter e on e.id = g.encounter_id
WHERE e.date_of_service >= '01-01-2020'
  AND p.problem_type_id IS NULL
ORDER BY g.encounter_id, p.id, g.id, a.id ASC;

-- Custom goal care plans
SELECT g.encounter_id, pc.subcategory, pt.label as problem, g.custom_label as goal, a.supplement as action
FROM clinical.problem p
JOIN clinical.problem_category pc ON pc.id = p.problem_category_id
JOIN clinical.problem_type pt ON pt.id = p.problem_type_id
JOIN clinical.goal g ON g.problem_id = p.id
JOIN clinical.action a ON a.goal_id = g.id
JOIN aspire.encounter e on e.id = g.encounter_id
WHERE e.date_of_service >= '01-01-2020'
  AND g.goal_type_id IS NULL
ORDER BY g.encounter_id, p.id, g.id, a.id ASC;